defmodule HelloWeb.PageController do
  use HelloWeb, :controller

  def index(conn, _params) do
    IO.inspect(conn)
    render(conn, "index.html")
  end

  def say_hello(conn, _params) do
    text conn, "Hello!"
  end

  def login(conn, _params) do
    render conn
  end

  def signup(conn, _params) do
    render conn
  end

  def show(conn, %{"messenger" => messenger}) do
    render(conn, "show.html", messenger: messenger)
  end

end
