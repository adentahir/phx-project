defmodule HelloWeb.HelloController do
  use HelloWeb, :controller

  def show(conn, %{"messenger" => messenger}) do
    text(conn, "From messenger #{messenger}")
  end

  def index(conn, _params) do conn
  |> put_flash(:info, "Welcome to Phoenix, from flash info!")
  |> put_flash(:error, "Let's pretend we have an error.")
   # redirect(conn, to: Routes.page_path(conn, :redirect_test))
    redirect(conn, external: "https://elixir-lang.org/")
  end

  def redirect_test(conn, _params) do
    render(conn, "index.html")
  end

end
