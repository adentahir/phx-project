defmodule HelloWeb.ImageController do
  use HelloWeb, :controller

  def say_hello(conn, _params) do
    text conn, "Hello!"
  end
end
